angular.module('sportsStore', ['customFilters', 'cart', 'ngRoute'])
.config(function ($routeProvider) {
    
    $routeProvider.when('/complete', {
        templateUrl: "/views/thankYou.html"
    });
    
    $routeProvider.when('/placeorder', {
        templateUrl: "/views/placeOrder.html"
    });
    
    $routeProvider.when('/checkout', {
        templateUrl: "/views/checkoutSummary.html"
    });
    
    $routeProvider.when('/products', {
        templateUrl: "/views/productLists.html"
    });
    
    $routeProvider.otherwise({
        templateUrl: "/views/productLists.html"
    });
    
})
.controller('mainController', function($scope){
    $scope.data = {
        products: [
            { id:1, name: "Product #1", description: "A Product", 
                category: "Category #1", price: 100 },
            { id:2, name: "Product #2", description: "A Product",
                category: "Category #1", price: 110 },
            { id:3, name: "Product #3", description: "A product",
                category: "Category #2", price: 210 },
            { id:4, name: "Product #4", description: "A product",
                category: "Category #3", price: 202 }
        ]
    };
})
.constant('productListActiveClass', 'btn-primary')
.constant('productPageCount', 3)
.controller('productListCtrl', function ($scope, $filter, productListActiveClass, productPageCount, cart){
    var selectedCategory = null;
    
    $scope.selectedPage = 1;
    $scope.pageSize = productPageCount;
    
    $scope.selectCategory = function (newCategory) {
        selectedCategory = newCategory;
        $scope.selectedPage = 1;
    }
    
    $scope.selectPage = function (newPage) {
        $scope.selectedPage = newPage;
    }
    
    $scope.categoryFilterFn = function (product) {
        return selectedCategory == null ||
            product.category == selectedCategory;
    }
    
    $scope.getCategoryClass = function (category) {
        return selectedCategory == category ? productListActiveClass : '';
    }
    
    $scope.getPageClass = function (page) {
        return $scope.selectedPage == page ? productListActiveClass : '';
    }
    
    $scope.addProductToCart = function (product) {
        cart.addProduct(product.id, product.name, product.price);
    }
})

